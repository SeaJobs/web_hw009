
import MyNavBar from "./components/MyNavBar";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import MyCard from "./components/MyCard";
import { Container } from "react-bootstrap";
import Account from "./pages/Account";
import Auth from "./pages/Auth";
import Video from "./pages/Video";
import Welcome from "./pages/Welcome";
import Detail from "./pages/Detail";
export default function App(props) {
console.log(props.id)
  return (
    <Container>
      <Router>
        <MyNavBar />
       
        <Switch>
          <Route exact path="/" component = {MyCard}/>
          <Route path="/account" component={Account} />
          <Route path="/auth" component={Welcome} />
          <Route path="/video" component={Video} />
          <Route path="/welcome" component={Welcome} />
          <Route path="/detail/:id" component ={Detail}/>
        </Switch>
    
      </Router>

    </Container>
  );
}
