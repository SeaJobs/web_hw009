import React from "react";
import {useState} from 'react'
import { Card, Row , Nav } from "react-bootstrap";
import {Link, useParams} from 'react-router-dom'

function Home() {
    const [CardInfo, setCardInfo] = useState([

        {
          id: 1,
          imgURL: "./images/benten.jpeg",
          title: "seven Deadly Sins",
          content: "Content",
          detail: "/Detail/1"
        },
        {
          id: 2,
          imgURL: "./images/benten2.jpg",
          title: "seven Deadly Sins",
          content: "Content",
          detail: "/Detail/2"
        },
        {
          id: 3,
          imgURL: "./images/benten3.jpg",
          title: "seven Deadly Sins",
          content: "Content",
          detail: "/Detail/3"
        },
        {
          id: 4,
          imgURL: "./images/benten4.jpeg",
          title: "seven Deadly Sins",
          content: "Content",
          detail: "/Detail/4"
        },
        {
          id: 5,
          imgURL: "./images/benten5.jpeg",
          title: "seven Deadly Sins",
          content: "Content",
          detail: "/Detail/5"
        },
        {
          id: 6,
          imgURL: "./images/benten6.jpeg",
          title: "seven Deadly Sins",
          content: "Content",
          detail: "/Detail/6"
        },
      ]);
      let {id} = useParams()

  return (
    <Row>
     
       {CardInfo.map((cardObj) => (
        <Card style={{ width: "300px" }} className="my-5">
          <Card.Img variant="top" src={cardObj.imgURL} />
          <Card.Body>
            <Card.Title>{cardObj.title}</Card.Title>
            <Card.Text>{cardObj.content}</Card.Text>
            <Nav.Link as = {Link} to = {cardObj.detail}>Read</Nav.Link>
           
          </Card.Body>
        </Card>
        
      ))}

    </Row>
     
  );

}

export default Home;
