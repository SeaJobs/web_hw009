import React from "react";
import { Nav,Navbar,Form,FormControl,Button } from "react-bootstrap";
import {Link} from 'react-router-dom';
function MyNavBar() {
  return (
    <Navbar bg="light" expand="lg">
        <Navbar.Brand href="#">React-Router</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="mr-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link as = {Link} to ="/">Home</Nav.Link>
            <Nav.Link as = {Link} to ="/video">Video</Nav.Link>
            <Nav.Link as = {Link} to ="/account">Account</Nav.Link>
            <Nav.Link as = {Link} to ="/welcome">Welcome</Nav.Link>
            <Nav.Link as = {Link} to ="/welcome">Auth</Nav.Link>
            
            <Form className="d-flex">
              <FormControl
                type="search"
                placeholder="Search"
                className="mr-2"
                aria-label="Search"
              />
              <Button variant="outline-success">Search</Button>
            </Form>
        
          </Nav>
        </Navbar.Collapse>
      </Navbar>
  );
}

export default MyNavBar;
