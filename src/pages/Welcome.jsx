import React from "react";
import { Form, Button,Nav } from "react-bootstrap";
import {Link} from 'react-router-dom';

function Welcome() {
  return (
    <div>
      
      <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" />
          <Form.Text className="text-muted">
            We'll never share your email with anyone else.
          </Form.Text>
        </Form.Group>

        <Form.Group className="mb-3" controlId="formBasicPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formBasicCheckbox">
          <Form.Check type="checkbox" label="Check me out" />
        </Form.Group>
        <Nav.Link as = {Link} to = "/welcome">
          <Button variant="primary">LogIn</Button>
        </Nav.Link>
        
      </Form>
    </div>
  );
}

export default Welcome;
