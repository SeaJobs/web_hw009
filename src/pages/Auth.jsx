import React from "react";
import { Container } from "react-bootstrap";
import {Route} from 'react-router-dom';
import Welcome from './Welcome';

function Auth() {
  
  return (
    <Container>
       <Route path="/welcome" component={<Welcome/>}/>
    </Container>
  );
}

export default Auth;
