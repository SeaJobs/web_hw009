import React from "react";
import {Nav} from 'react-bootstrap'
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
function Account() {
  let {account} = useParams()
  return (
    <div>
      <Nav className="flex-column">
        <Nav.Link as = {Link} to = "/facebook">Facebook.com {account}</Nav.Link>
        <Nav.Link eventKey="/link-1">Instagram</Nav.Link>
        <Nav.Link eventKey="link-2">LinkIn</Nav.Link>
        <Nav.Link eventKey="Link-3">Google.com</Nav.Link>
      </Nav>
    </div>
  );
}


export default Account;
