import React from 'react'
import {ButtonGroup,Button} from 'react-bootstrap';

function OnMovie() {
    return (
        <div>

        <ButtonGroup aria-label="Basic example">
        <Button variant="secondary">Action</Button>
        <Button variant="secondary">Romance</Button>
        <Button variant="secondary">Category</Button>
      </ButtonGroup>
            
        </div>
    )
}

export default OnMovie
