import React from "react";
import { Button, ButtonGroup ,Nav} from "react-bootstrap";
import { Link, Router, useHistory} from "react-router-dom";
import {BrowserRouter as Route , Switch } from 'react-router-dom'

function Video({match}) {

  return (
    <div>
      <h1>Video</h1>

      <Nav.Link as={Link} to="/video">
        <ButtonGroup aria-label="Basic example">
          <Nav.Link as={Link} to="/movie">
            <Button variant="secondary" onClick={onMovie()}>
              Movie
            </Button>
          </Nav.Link>
          <Nav.Link as={Link} to="/animation">
            <Button variant="secondary" onClick={onAnimation()}>
              Animation
            </Button>
          </Nav.Link>
          <Nav.Link as={Link} to="/cartoon">
            <Button variant="secondary" onClick={onCartoon()}>
              Cartoon
            </Button>
          </Nav.Link>
        </ButtonGroup>
      </Nav.Link>
    </div>
  );

  function onMovie() {

    return(
      <Link to="/movie">
      <ButtonGroup aria-label="Basic example">
        <Button variant="secondary">Action</Button>
        <Button variant="secondary">Romance</Button>
        <Button variant="secondary">Category</Button>
      </ButtonGroup>
    </Link>
    )

  }
  
  function onAnimation() {
    <Link to="/animation">
      <ButtonGroup aria-label="Basic example">
        <Button variant="secondary">Action</Button>
        <Button variant="secondary">Romance</Button>
        <Button variant="secondary">Category</Button>
      </ButtonGroup>
    </Link>;
  }
  
  function onCartoon() {
    <Link to="/cartoon">
      <ButtonGroup aria-label="Basic example">
        <Button variant="secondary">Action</Button>
        <Button variant="secondary">Romance</Button>
        <Button variant="secondary">Category</Button>
      </ButtonGroup>
    </Link>;

  }

//   function Topic() {
//     let { topicId } = useParams();
//     return <h3>Requested topic ID: {topicId}</h3>;
//   }

// function Topics() {
//     let match = useRouteMatch();
  
//     return (
//       <div>
//         <h2>Topics</h2>
  
//         <ul>
//           <li>
//             <Link to={`${match.url}/components`}>Components</Link>
//           </li>
//           <li>
//             <Link to={`${match.url}/props-v-state`}>
//               Props v. State
//             </Link>
//           </li>
//         </ul>
  
//         {/* The Topics page has its own <Switch> with more routes
//             that build on the /topics URL path. You can think of the
//             2nd <Route> here as an "index" page for all topics, or
//             the page that is shown when no topic is selected */}
//         <Switch>
//           <Route path={`${match.path}/:topicId`}>
//             <Topic />
//           </Route>
//           <Route path={match.path}>
//             <h3>Please select a topic.</h3>
//           </Route>
//         </Switch>
//       </div>
//     );
//   }
  
}


export default Video;
